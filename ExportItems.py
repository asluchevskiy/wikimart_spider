# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import logging
import sys
import locale
import pymongo
import csv
import os
import trans
from jinja2 import Template
from pyquery import PyQuery
from utils import make_path_from_url
from grab.tools.text import normalize_space
try:
    import local_settings as settings
except ImportError:
    import settings

class ExportItems():
    def __init__(self):
        self.db = pymongo.Connection()[settings.MONGO_DB]
        with open(os.path.join(os.path.dirname(__file__), 'templates/description.html'), 'r') as f:
            self.template = Template(f.read())

    categories = list()

    def export_all(self):
        logging.info('Exporting all sites')
        for site in self.db.site.find():
            self.export(site['name'])

    def export(self, site_name):
        self.site_name = site_name
        site = self.db.site.find_one({'name':site_name})
        if not site:
            logging.error('Could not find site with name %s' % site_name)
            return
        logging.info('Exporting CSV for site %s' % site_name)
        f = open(os.path.join(settings.DATA_PATH, '%s.csv' % self.site_name), 'w')
        self.rsync_list = open(os.path.join(settings.DATA_PATH, '%s.list' % self.site_name), 'w')
        self.w = csv.writer(f, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        self.write_header()

        for url in site['categories']:
            path = make_path_from_url(url)
            self.write_items(path, 0)

        f.close()
        self.rsync_list.close()

    def write_header(self):
        self.w.writerow(
        [
            u'Наименование (Русский)',
            u'ID страницы (часть URL; используется в ссылках на эту страницу)',
            u'Артикул',
            u'Цена',
#            u'Название вида налогов',
            u'Скрытый',
            u'Можно купить',
#            u'Старая цена',
#            u'На складе',
#            u'Продано',
            u'Описание (Русский)',
            u'Краткое описание (Русский)',
#            u'Сортировка',
            u'Заголовок страницы (Русский)',
#            u'Тэг META keywords (Русский)',
#            u'Тэг META description (Русский)',
#            u'Стоимость упаковки',
#            u'Вес продукта',
#            u'Бесплатная доставка',
#            u'Ограничение на минимальный заказ продукта (штук)',
#            u'Файл продукта',
#            u'Количество дней для скачивания',
#            u'Количество загрузок (раз)',
        ] + [u'Фотография'] * 10
        )

    def write_items(self, path, level):
        self.w.writerow([
            '!'*level + self.db.cat.find_one({'path':path})['name'],
        ])
        for item in self.db.item.find({'is_parsed':True, 'path':path}, timeout=False):
            if item['description']:
                descr_text = PyQuery(item['description']).text()
                description = normalize_space(self.template.render(item=item))
            else:
                description = ''
                descr_text = ''
            if item['is_downloaded']:
                images_list = [
                    '%s,%s,%s' % ( os.path.basename(item['local_images'][key]['normal']),
                                   os.path.basename(item['local_images'][key]['thumb']),
                                   os.path.basename(item['local_images'][key]['full']) )
                    for key in item.get('local_images', {})
                ]
                for key in item.get('local_images', {}):
                    for image_type in ('normal', 'thumb', 'full'):
                        self.rsync_list.write(item['local_images'][key][image_type])
                        self.rsync_list.write('\n')
            else:
                images_list = []
            self.w.writerow([
                item['name'],
                item['name'].encode('trans/slug').lower(),
                item['model_id'],
                item['price']*settings.PRICE_CHANGE,
                0,
                1,
                description,
                descr_text,
                item['name'],
            ] + images_list)
        for cat in self.db.cat.find():
            if cat['path'][:len(path)] == path and len(cat['path']) == len(path)+1:
                self.write_items(cat['path'], level=level+1)

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding(locale.getpreferredencoding())
    if settings.DEBUG:
        logging.basicConfig(level=logging.DEBUG)

    logging.debug('Exporting...')
    ExportItems().export('testshop.com')
