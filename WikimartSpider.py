# -*- coding: utf-8 -*-
import os
import re
import logging
import datetime
import signal
import pymongo
import json
import base64
import shutil
import pexpect
import sys
from grab.spider import Spider, Task
from urllib import urlencode
from hashlib import sha1
from grab.tools.text import normalize_space
from utils import make_path_from_url
from PIL import Image
try:
    import local_settings as settings
except ImportError:
    import settings

class WikimartBaseSpider(Spider):

    def __init__(self, site_name = None, **kwargs):
        self.site_name = site_name
        super(WikimartBaseSpider, self).__init__(**kwargs)

    def shutdown(self):
        print self.render_stats()

    def sigint_handler(self, signal, frame):
        logging.info('Interrupting...')
        self.should_stop = True

    def prepare(self):
        signal.signal(signal.SIGINT, self.sigint_handler)
        if settings.PROXY_LIST:
            self.setup_proxylist(settings.PROXY_LIST, 'http', auto_change=False)
        if settings.USE_CACHE:
            self.setup_cache(database=settings.MONGO_DB)
        self.db = pymongo.Connection()[settings.MONGO_DB]
        self.now = datetime.datetime.now()

        if not self.site_name:
            logging.info('Parsing all sites')
            self.site = None
        else:
            self.site = self.db.site.find_one({'name':self.site_name})
            if not self.site:
                logging.error('Could not find site %s' % self.site_name)
                exit()
            logging.info('Parsing site %s' % self.site_name)

        if self.site:
            self.search_query = {'$or':[]}
            for url in self.site['categories']:
                path = make_path_from_url(url)
                d = dict()
                for i in range(len(path)):
                    d['path.%s' % i] = path[i]
                self.search_query['$or'].append(d)
        else:
            self.search_query = {}

    def shutdown(self):
        pass

    def build_cache_hash(self, url):
        return sha1(url).hexdigest()

class WikimartImagesSpider(WikimartBaseSpider):

    images_count = {}

    def check_image_done(self, id):
        if self.images_count.has_key(id) and self.images_count[id] == 0:
#            logging.debug('Marking %s as downloaded' % id)
            self.db.item.update({'_id':id}, {'$set': {'is_downloaded':True}})
            del self.images_count[id]

    def task_generator(self):
        self.search_query.update({'is_downloaded':False, 'is_parsed':True})
        for item in self.db.item.find(self.search_query, timeout=False):
            self.images_count[item['_id']] = len(item['images'])
            for i in range(len(item['images'])):
                yield Task('image', url=item['images'][i]['full'], id=item['_id'], no=i)
            self.check_image_done(item['_id'])

    def task_image(self, grab, task):
        if not self.images_count.has_key(task.id):
            return
        _hash = self.build_cache_hash(task.url)

        name_list = list()
        path_list = list()
        for image_type in ('full', 'normal', 'thumb'):
            name_list.append('%s_%s.jpeg' % (_hash, image_type))
            path_list.append(os.path.join(settings.IMAGE_PATH, name_list[-1]))
        grab.response.save(path_list[0], create_dirs=True)

        for i in (1,2):
            try:
                im = Image.open(path_list[0])
                if im.mode != "RGB":
                    im = im.convert("RGB")
                image_size = settings.IMAGE_NORMAL_SIZE if i == 1 else settings.IMAGE_THUMB_SIZE
                im.thumbnail(image_size, Image.ANTIALIAS)
                im.save(path_list[i], 'JPEG', quality=settings.IMAGE_JPEG_QUALITY)
            except IOError:
                os.remove(path_list[0])
                return

        self.db.item.update({'_id':task.id}, {'$set': {
            'local_images.%s'%task.no : {
                'full' : name_list[0],
                'normal' : name_list[1],
                'thumb' : name_list[2],
            }
        }})
        self.images_count[task.id]-=1
        self.check_image_done(task.id)

class WikimartItemsSpider(WikimartBaseSpider):

    def task_generator(self):
        self.search_query.update({'is_parsed':False})
        for item in self.db.item.find(self.search_query, timeout=False):
#        for item in self.db.item.find({}, timeout=False):
            try:
                yield Task('page', url = item['url'], id=item['_id'])
            except KeyError:
                print item

    def task_page(self, grab, task):
        if grab.response.code == 404:
            self.db.item.remove({'_id':task.id})
            return

        item = self.db.item.find_one({'_id':task.id})
        if not item:
            return
        try:
            pq = grab.pyquery
        except ValueError:
            yield task.clone(refresh_cache=True); return
        page_url = pq.find('meta[property="og:url"]').attr('content')
        if not page_url:
            logging.error('Wrong page: %s' % task.url)
            yield task.clone(refresh_cache=True); return

        new_id = int(re.search('\/model\/(\d+)\/', page_url).group(1))
        if new_id != item['model_id']: # переехавший товар
            logging.info('Item was moved at wikimart: %s' % task.url)
            item['model_id'] = new_id

        item.update({
            'mtime' : datetime.datetime.now(),
            'url' : page_url,
#            'path' : make_path_from_url(page_url),
            'name' : pq.find('h1').text(),
            'is_parsed' : True
        })

        if pq.find('ul#AllGood'):
            item['is_in_stock'] = True
            item['price'] = int(pq.find('ul#AllGood li').eq(0).attr('data-price'))
        else:
            item['is_in_stock'] = False

        if pq.find('div.PathLine a').eq(1):
            item['brand'] = pq.find('div.PathLine a').eq(1).attr('href').split('brand')[-1].strip('/')
        else:
            item['brand'] = None
        if pq.find('div.Content div.description'):
            item['description'] = normalize_space(pq.find('div.Content div.description').html().strip())
        else:
            item['description'] = None

        item['properties'] = list()
        dl_list = pq.find('div.Props dl.ui-helper-clearfix')
        for i in range(len(dl_list)):
            item['properties'].append({
                'property' : dl_list.eq(i).find('dt').text(),
                'value' : dl_list.eq(i).find('dd').text(),
            })

        item['images'] = list()
        a_list = pq.find('div.SmallImg li a')
        for i in range(len(a_list)):
            item['images'].append({
                'full' : a_list.eq(i).attr('href'),
                'normal' : a_list.eq(i).attr('data-big-url'),
                'thumb' : a_list.eq(i).find('img').attr('src'),
            })
        if not len(item['images']):
            a = pq.find('div.Image a')
            img_normal = a.find('img').attr('src')
            if img_normal:
                item['images'].append({
                    'full' : a.attr('data-large-url'),
                    'normal' : img_normal,
                    'thumb' : None,
                })

        self.db.item.save(item)

class WikimartSpider(WikimartBaseSpider):

    initial_urls = ['http://wikimart.ru/catalog/']

    def prepare(self):
        super(WikimartSpider, self).prepare()
        if not self.site_name:
            self.sites = list(self.db.site.find())
        else:
            self.sites = list(self.db.site.find({'name':self.site_name}))

    def task_initial(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('a.main-category'):
            s = re.search('http://([a-z]+)\.', a.get('href'))
            if not s:
                continue
            slug = s.group(1)
            path = [ slug ]
            curl = a.get('href')
            cat = {'slug': slug, 'name' : unicode(a.text_content()), 'path' : path, 'url' : curl}
            if not self.db.cat.find_one(cat):
                self.db.cat.save(cat)
            if self.check_category(path):
                yield Task('category', url=curl, curl=curl, path=path, p=1)

    def check_category(self, path):
        for site in self.sites:
            for url in site['categories']:
                test_path = make_path_from_url(url)
                if len(test_path) > len(path):
                    if path == test_path[:len(path)]:
                        return True
                else:
                    if path[:len(test_path)] == test_path:
                        return True
        return False

    def task_category(self, grab, task):
        pq = grab.pyquery
        divs = pq.find('div.ListGood')
        if not divs:
            a_list = pq.find('h3.CatFirSub a')
        else:
            if pq.find('h2.HeadTit').text() == u'Подкатегории' and task.p==1:
                a_list = pq.find('ul.Category li.level-1 a')
            else:
                a_list = []
        for a in a_list:
            curl = grab.make_url_absolute(a.get('href')).split('?')[0]
            path = make_path_from_url(curl)
            slug = path[-1]
            cat = dict(slug=slug, name=unicode(a.text_content()), path=path, url=curl)
            if not self.db.cat.find_one(cat):
                self.db.cat.save(cat)
            if self.check_category(path):
                yield Task('category', url=curl+'?'+urlencode({'p':1,'list':1}), path=task.path + [slug], p=1, curl=curl)
        # мы дошли до последнего уровня подкатегории
        if divs and not a_list:
            dynamics_url = re.search('http:\/\/.+?\/tmp\/js_dynamics2/.+?\.js', grab.response.body).group(0)
            brands = dict()
            for a in pq.find('div.Bran li a'):
                brands[a.get('data-key')] = a.text_content()
            yield Task('js_dynamics', url=dynamics_url, path = make_path_from_url(task.url), brands=brands, curl=task.url.split('?')[0])
            yield Task('list', url=task.url, path=make_path_from_url(task.url), p=1, curl=task.url.split('?')[0], brand=None)

    def task_js_dynamics(self, grab, task):
        s = re.search('seoHrefs = ({.+?})', grab.response.body)
        if not s:
            return
        content = json.loads(s.group(1))
        for key in task.brands:
            if not content.has_key(key):
                continue
            curl = base64.decodestring(content[key]).decode('utf-8').split('?')[0]
            slug = curl.strip('/').split('/')[-1]
            path = task.path + [slug]
            cat = dict(slug=slug, path=path, name=task.brands[key], url=curl)
            if not self.db.cat.find_one(cat):
                self.db.cat.save(cat)
            yield Task('list', url=curl+'?'+urlencode({'p':1,'list':1}), p=1, curl=curl, path=path, brand=task.brands[key])

    def task_list(self, grab, task):
        pq = grab.pyquery
        divs = pq.find('div.ListGood')
        if divs:
            yield Task('list', url=task.curl+'?'+urlencode({'p':task.p+1,'list':1}),
                p=task.p+1, curl=task.curl, path=task.path, brand=task.brand)
        # товары
        for i in xrange(len(divs)):
            div = divs.eq(i)
            model_id = int(div.attr('model_id'))
            price = int( div.find('div.price').text()[:-3].replace(' ', '') )
            item = self.db.item.find_one({'model_id' : model_id}, {'_id':1, 'model_id':1, 'price':1, 'brand':1, 'path':1})
            if not item:
                data = {
                    'model_id' : model_id,
                    'url' : div.find('div.InfoModel a').attr('href'),
                    'name' : div.find('div.InfoModel a').text(),
                    'price' : price,
                    'brand' : task.brand,
                    'is_parsed' : False,
                    'is_downloaded' : False,
                    'path' : task.path,
                    'ctime' : datetime.datetime.now(),
                    'mtime' : datetime.datetime.now(),
                }
                self.db.item.save(data)
            else:
                self.db.item.update({'_id':item['_id']}, {'$set': {
                    'price':price,
                    'brand' : task.brand if not item['brand'] else item['brand'],
                    'path' : task.path if not item['brand'] else item['path'],
                    'mtime':datetime.datetime.now()
                }})

class Cleaner(object):
    def __init__(self):
        self.db = pymongo.Connection()[settings.MONGO_DB]

    def run(self):
        logging.info('Cleaning all data...')
        self.db.cat.remove()
        self.db.item.remove()
        try:
            shutil.rmtree(settings.IMAGE_PATH)
        except (IOError, OSError):
            pass
        logging.info('Done!')

class Rsync(object):

    def __init__(self):
        self.db = pymongo.Connection()[settings.MONGO_DB]

    def rsync_all(self):
        logging.info('Rsyncing all sites')
        for site in self.db.site.find():
            self.rsync(site['name'])

    def rsync(self, site_name):
        def make_path(path):
            if path and path[-1] == '/':
                return path
            else:
                return path + '/'

        site = self.db.site.find_one({'name':site_name})
        if not site:
            logging.error('Could not find site %s' % site_name)
            return
        cmd  = 'rsync -ahv -rsh=\'ssh\' --files-from=%s %s %s@%s:%s' % (
            os.path.join(settings.DATA_PATH, '%s.list' % site_name), # filelist
            settings.IMAGE_PATH, # images dir
            site['login'], # login
            site['host'], # host
            make_path(site['path'])  # remote path
        )
#        print cmd
        child = pexpect.spawn(cmd)
        child.logfile = sys.stdout
        try:
            child.expect('Are you sure you want to continue connecting (yes/no)?', timeout=1)
            child.sendline('yes')
        except pexpect.TIMEOUT:
            pass
        child.expect('password:')
        child.sendline(site['password'])
        child.expect(pexpect.EOF, timeout=None)
#        print child.before
