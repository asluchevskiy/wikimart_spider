# -*- coding: utf-8 -*-
import os
import sys
import logging
import locale
import optparse
import socket

from WikimartSpider import WikimartSpider, WikimartItemsSpider, WikimartImagesSpider, Cleaner, Rsync
from ExportItems import ExportItems

try:
    import local_settings as settings
except ImportError:
    import settings

def run_categories():
    WikimartSpider(thread_number=settings.THREADS, site_name=options.site).run()

def run_items():
    WikimartItemsSpider(thread_number=settings.THREADS, site_name=options.site).run()

def run_images():
    WikimartImagesSpider(thread_number=settings.THREADS, site_name=options.site).run()

def run_export():
    if not options.site:
        ExportItems().export_all()
    else:
        ExportItems().export(options.site)

def run_clear():
    Cleaner().run()

def run_rsync():
    if not options.site:
        Rsync().rsync_all()
    else:
        Rsync().rsync(options.site)

if __name__ == '__main__':
    # hack for unicode console i/o
    reload(sys)
    sys.setdefaultencoding(locale.getpreferredencoding())
    if settings.DEBUG:
        logging.basicConfig(level=logging.DEBUG)

    parser = optparse.OptionParser("usage: %prog [options]")
    parser.add_option('-m', '--mode', action="store", dest="mode", default=None, help='mode to run: all|clear|categories|items|images|export|rsync')
    parser.add_option('-s', '--site', action="store", dest="site", default=None, help='site to parse')
    (options, args) = parser.parse_args()

    if options.mode == 'all':
        run_categories(); run_items(); run_images(); run_export(); exit()
    if options.mode == 'categories':
        run_categories(); exit()
    elif options.mode == 'items':
        run_items(); exit()
    elif options.mode == 'images':
        run_images(); exit()
    elif options.mode == 'clear':
        run_clear(); exit()
    elif options.mode == 'export':
        run_export(); exit()
    elif options.mode == 'rsync':
        run_rsync(); exit()
    else:
        parser.print_help(); exit()
