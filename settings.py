# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import os

#PROXY_LIST = os.path.join(os.path.dirname(__file__), 'ru_proxy.list')
PROXY_LIST = None

DEBUG_LOG_DIR = os.path.join(os.path.dirname(__file__), 'logs')
DATA_PATH = os.path.join(os.path.dirname(__file__), 'data')
IMAGE_PATH = os.path.join(os.path.dirname(__file__), 'images')

USE_CACHE = False
MONGO_DB = 'wikimart'

IMAGE_THUMB_SIZE = (100, 100)
IMAGE_NORMAL_SIZE = (200, 200)
IMAGE_JPEG_QUALITY = 85

PRICE_CHANGE = 1.05 # +5%

DEBUG = True
THREADS = 32
