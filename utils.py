# -*- coding: utf-8 -*-
import datetime
from urlparse import urlparse

def make_path_from_url(url):
    u = urlparse(url)
    path = [ u.netloc.split('.')[0] ] + u.path.strip('/').split('model')[0].strip('/').split('/')
    if path[-1] == '':
        return path[:-1]
    else:
        return path

def raw_date_to_date(raw_date):
    s = unicode(raw_date).split()
    month_to_date = {
        u'января' : 1,
        u'февраля' : 2,
        u'марта' : 3,
        u'апреля' : 4,
        u'мая' : 5,
        u'июня' : 6,
        u'июля' : 7,
        u'августа' : 8,
        u'сентября' : 9,
        u'октября' : 10,
        u'ноября' : 11,
        u'декабря' : 12,
    }
    month = month_to_date[ s[1].lower() ]
    day = int( s[0] )
    year = int( s[2] )
    return datetime.datetime(year=year, month=month, day=day)

class dotdict(dict):
    def __getattr__(self, attr):
        return self.get(attr, None)
    __setattr__= dict.__setitem__
    __delattr__= dict.__delitem__
