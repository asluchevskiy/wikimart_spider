# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import pymongo
import sys
import os
import string
import dateutil.parser
from datetime import timedelta, datetime
from bson import ObjectId
from utils import dotdict
from flask import Flask, render_template, g, abort, request
from wtforms import validators, Form, TextField, TextAreaField, PasswordField, HiddenField, BooleanField

try:
    import local_settings as settings
except ImportError:
    import settings

app = Flask(__name__)

class SiteForm(Form):
    id = HiddenField()
    name = TextField(u'Имя сайта', [validators.Required()])
    categories = TextAreaField(u'Категории', description=u'Ссылки на категории wikimart, по ссылке на строку') # TODO: custom field
    url = TextField(u'URL', [validators.URL(), validators.Required()])
    host = TextField(u'Host')
    login = TextField(u'Login')
    password = PasswordField(u'Password')
    path = TextField(u'Путь до изображений')

    def process(self, formdata=None, obj=None, **kwargs):
        if obj:
#            obj.categories = string.join(obj.categories, '\n')
            obj.id = obj._id
        super(SiteForm, self).process(formdata=formdata, obj=obj, **kwargs)

    def validate_categories(self, field):
        pass

@app.template_filter('format_seconds')
def format_seconds(value):
    return timedelta(seconds=int(value)) if value else ''

@app.before_request
def setup_db():
    g.db = pymongo.Connection()[settings.MONGO_DB]

@app.route('/')
def hello():
    obj = g.db.site.find_one()
    form = SiteForm(obj=dotdict(obj))
    return render_template('index.html', sites=g.db.site.find(), form=form)

# TODO: mongodb wrapper, bootstrap templates, ajax! google for them!!!

@app.route('/ajax/add/', methods=['GET', 'POST'])
def ajax_add():
    form = SiteForm(request.form)
    if request.method == 'POST' and form.validate():
        return str(form.data)
#        return 'OK'
    return ''

@app.route('/ajax/get_form/')
def get_ajax_form():
    return ''

if __name__ == "__main__":
    app.run(debug = True, host = '0.0.0.0')
